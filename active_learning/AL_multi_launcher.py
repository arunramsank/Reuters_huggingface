import os
from multiprocessing import Pool
import torch

# First try to kill all running process with
try:
    os.system("nvidia-smi | grep 'python' | awk '{ print $3 }' | xargs -n1 kill -9")
    print("All running GPU jobs were killed.")
except:
    print("Currently, no running GPU jobs. You are good to go.")

num_gpus = torch.cuda.device_count()
gpu_ids = tuple(map(str, list(range(num_gpus))))


def run_process(gpu_id):
    os.system(f"python AL.py --gpu_id {gpu_id}")


pool = Pool(processes=len(gpu_ids))
pool.map(run_process, gpu_ids)
