import os
import subprocess
import time
import sys
import pandas as pd
import time
import datetime
import random
import torch
import pickle

import argparse

parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)

parser.add_argument(
    "--gpu_id", type=str, default=0, help="The GPU to use for training, default is 0,"
)

gpu_id = parser.parse_args().gpu_id

if torch.cuda.device_count() <= int(gpu_id):
    raise ValueError(f"Selected GPU {gpu_id} does not exist.")


def make_call_string(arglist):
    result_string = ""
    for arg in arglist:
        result_string += "".join(["--", arg[0], " ", str(arg[1]), " "])
    return result_string


cwd = os.path.dirname(os.path.abspath(__file__))
root = os.path.dirname(cwd)
data_dir = os.path.join(root, "glue_data", "Reuters")
glue_dir = os.path.join(root, "examples", "text-classification")

warmup_steps_list = [200]
max_steps_list = [600]
logging_steps_list = [150]


max_seq_length_list = [256]
model_list = [
    ("albert-base-v2", 32),
    # ("albert-large-v2", 8),
    ("distilbert-base-cased", 32),
    # ("xlm-roberta-base", 16),
    ("roberta-base", 32),
    ("bert-base-cased", 32),
    ("xlnet-base-cased", 16),
    # ("bert-base-uncased", 32),
]
learning_rate_list = [2e-5]
seed_list = [False]  # [True, False]

subset_size_list = [1, 2, 5, 10, 20, 50]

train_size = 4659

output_dir = os.path.join(data_dir, "model_checkpoints")

for _ in range(1000):
    model = random.choice(model_list)
    model_name_or_path, per_device_train_batch_size = model
    warmup_steps = random.choice(warmup_steps_list)
    max_seq_length = random.choice(max_seq_length_list)
    learning_rate = random.choice(learning_rate_list)
    seed = random.choice(seed_list)
    max_steps = random.choice(max_steps_list)
    subset_size = random.choice(subset_size_list)
    logging_steps = random.choice(logging_steps_list)

    if seed:
        seed_val = 42
    else:
        seed_val = int(time.time() * 10000) % 100000
    wandb_name = "_".join(map(str, [model_name_or_path,],))

    arglist = [
        ["task_name", "Reuters"],
        ["do_train", ""],
        ["do_eval", ""],
        # ["do_predict", ""],
        ["evaluate_during_training", ""],
        ["data_dir", data_dir],
        ["model_name_or_path", model_name_or_path],
        # ["model_name_or_path", output_dir],
        ["per_device_train_batch_size", per_device_train_batch_size],
        ["max_seq_length", max_seq_length],
        ["learning_rate", learning_rate],
        ["logging_steps", logging_steps],
        ["output_dir", output_dir + gpu_id],
        ["overwrite_output_dir", ""],
        ["warmup_steps", warmup_steps],
        ["max_steps", max_steps],
        ["wandb_name", wandb_name],
        ["subset_size", subset_size],
        # ["labeled_file", labeled_file],
        ["train_size", train_size],
        ["seed", seed_val],
    ]

    call_string = " ".join(
        [
            f"CUDA_VISIBLE_DEVICES={gpu_id} python {os.path.join(glue_dir, 'run_glue.py')}",
            make_call_string(arglist),
        ]
    )

    print("Running Script: \n\n\n", call_string, "\n")
    start = time.time()
    subprocess.call(call_string, shell=True)
    end = time.time()
    time.sleep(random.randint(10, 50))
    os.system(f"wandb sync")
    print("Run completed in {0:.1f} seconds".format(end - start))
