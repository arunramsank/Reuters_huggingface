# Active Learning Experiments

We compare different embedding models and their effect on model performance when trained on the Reuters classification task. A write-up of the results is available [here](https://docs.google.com/document/d/10j30ewBPFIL-CMh-0J9Q087902rhx7txh623KLHWuKY/edit?usp=sharing).

## Setup

Follow the setup instructions in the main [README.md](./../README.md).


## Running Experiments

To run different experiments set your desired values to pick from for a random search. 

Possible model parameters and suggested optimal values are:

| Model Parameter    | Recommended Values  |
|-----------|------------------------------------------------------|
| (model, batchsize)         |  ("roberta-base", 32) or any other classification model at [huggingface.co/models](https://huggingface.co/models)  |
| learning_rate | 2e-5 |
| warmup_steps   | 200      |
| max_steps   | 600      |
| logging_steps   | 150      |
| max_seq_length_list   | 256      |
| subset_size   | int between 0 and 100    |

To start the random search, simply run:

```
python AL.py
```
## Running Experiments on Multiple GPUs
If your machine has multiple GPUs, you can run experiments on each machine with 

```
python AL_multi_launcher.py
```
Of course, you can also do distributed training across multiple GPUs but in this case, you need to find your own optimal hyperparameters.